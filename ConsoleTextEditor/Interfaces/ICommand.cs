﻿
using Microsoft.Extensions.Logging;

namespace ConsoleTextEditor.Interfaces
{
	/// <summary>
	/// Command.
	/// </summary>
	public interface ICommand
	{
		/// <summary>
		/// Description. 
		/// </summary>
		string CommandName { get; }

		/// <summary>
		/// Command  code.
		/// </summary>
		string Command{ get; }

		/// <summary>
		/// Execute command.
		/// </summary>
		/// <param name="core">Editor core.</param>
		/// <param name="command">Command parameters.</param>
		void Execute(ICore core, string[] command);

		/// <summary>
		/// Log action and errors.
		/// </summary>
		ILogger Logger { get; set; }
	}
}