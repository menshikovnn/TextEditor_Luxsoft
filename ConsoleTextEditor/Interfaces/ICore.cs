﻿using System;
using System.Collections.Generic;

namespace ConsoleTextEditor.Interfaces
{
	public interface ICore
	{
		/// <summary>
		/// Stop flag;
		/// </summary>
		bool Stop { get; set; }

		/// <summary>
		/// Prefics  for UI.
		/// </summary>
		string Prefics { get; }

		/// <summary>
		/// Lines.
		/// </summary>
		LinkedList<string> FileLines { get; set; }

		/// <summary>
		/// File name on load file as paramenter.
		/// </summary>
		/// <param name="fileName"></param>
		string FileName { get; }

		/// <summary>
		/// Start process with parameter.
		/// </summary>
		/// <param name="fileName">file name.</param>
		void Start(string fileName);

		/// <summary>
		/// Start process without parameter.
		/// </summary>
		void Start();

		/// <summary>
		/// Flag change, if lines list was change.
		/// </summary>
		bool FileChanged { get; set; }

	}
}
