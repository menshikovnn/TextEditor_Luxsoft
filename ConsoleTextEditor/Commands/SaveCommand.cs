﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Save data to file
	/// </summary>
	public class SaveCommand : ICommand
	{
		public string CommandName => "Save file. Use save filename. If filename - name of new file. File extension can be omitted. \r\n If text file was load as parameter use save command without parameter.  ";

		public string Command => "save";

		public ILogger Logger { get; set; }
		public SaveCommand (ILogger<Core> logger)
		{
			Logger = logger;
		}
		public void Execute(ICore core, string[] command)
		{
			if (String.IsNullOrEmpty(core.FileName) && command.Length == 1)
			{
				Console.WriteLine($"{core.Prefics} File name not found!");
			}

			if (command.Length == 2)
			{
				core.FileChanged = !SaveFile(core.FileLines, command[1]);
			}
			else if (!String.IsNullOrEmpty(core.FileName))
			{
				core.FileChanged = !SaveFile(core.FileLines, core.FileName);
			}
		}

		/// <summary>
		/// Save file by filename.
		/// </summary>
		/// <param name="fileLines">List strings.</param>
		/// <param name="fileName">File name.</param>
		/// <returns></returns>
		private bool SaveFile(LinkedList<string> fileLines, string fileName)
		{
			try
			{
				if (Path.GetExtension(fileName) == string.Empty) fileName += ".txt";
				File.WriteAllLines(fileName, fileLines);
				Console.WriteLine("File saved");
				return true;
			}
			catch(Exception ex)
			{
				Logger.LogError($"{ex.Message}, stackTrace: {ex.StackTrace} ");
				Console.WriteLine("File not saved");
				return false;
			}
		}
	}
}
