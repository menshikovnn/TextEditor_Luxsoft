﻿using ConsoleTextEditor.Helpers;
using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Add line with parameters
	/// </summary>
	public class AddLineCommand : ICommand
	{
		public string CommandName => "Insert line. Use ins n text, where n - index(if not set, line will be last element), text - text for insert";

		public string Command => "ins";

		public ILogger Logger { get; set ; }

		public AddLineCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}

		public void Execute(ICore core, string[] command)
		{
			if (command[1].IsDigit() && command.Length == 3)
			{
				core.FileLines.Insert(Convert.ToInt32(command[1]), command[2]);
				Logger.LogInformation($"Line inserted {command[2]}");
			}
			else
			{
				core.FileLines.AddLast(command[1]);
				Logger.LogInformation($"Line inserted {command[1]}");
			}

			core.FileChanged = true;
			Console.WriteLine("Line inserted");
		}
	}
}
