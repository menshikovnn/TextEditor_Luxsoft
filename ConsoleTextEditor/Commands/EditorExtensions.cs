﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConsoleTextEditor.Helpers
{
	public static class EditorExtensions
	{
		/// <summary>
		/// Check string is number;
		/// </summary>
		public static bool IsDigit(this string data)
		{
			Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
			return regex.IsMatch(data);
		}

		public static void Insert(this LinkedList<string> list, int index, string data)
		{
			/// for user frendly interface 
			if (index == 1) index = 0;
			int currentIndex = 0;
			var currentNode = list.First;
			if (index >= 0 && index < list.Count - 1)
			{
				while (currentNode != null && currentIndex <= index)
				{
					if (currentIndex == index)
					{
						list.AddBefore(currentNode, data);
					}
					currentIndex++;
				}
			}
			else
			{
				list.AddLast(data);
			}
		}

		/// <summary>
		/// Delete node from list by index.
		/// </summary>
		/// <param name="list">LinkedList.</param>
		/// <param name="index">Index.</param>
		public static void Delete(this LinkedList<string> list, int index)
		{
			// for userfrendly interface
			if (index > 0) index = --index;

			if (index >= 0 && index <= list.Count - 1)
			{
				var currentNode = list.First;
				for (int i = 0; i <= index && currentNode != null; i++)
				{
					if (i != index)
					{
						currentNode = currentNode.Next;
						continue;
					}

					list.Remove(currentNode);
				}
			}
		}
	}
}
