﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Return command list to console.
	/// </summary>
	public class HelpCommand : ICommand
	{
		public string CommandName => "Help";
		public string Command => "help";
		public ILogger Logger { get; set; }
		public HelpCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}
		public void Execute(ICore core, string[] command)
		{
			Console.WriteLine(core.Prefics + "Commands:");

			var commands = Program.ServiceProvider.GetServices(typeof(ICommand));
			foreach (ICommand item in commands)
				Console.WriteLine($"{item.Command} : {item.CommandName}");
		}
	}
}
