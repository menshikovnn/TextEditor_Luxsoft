﻿using ConsoleTextEditor.Helpers;
using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Delete line.
	/// </summary>
	public class DeleteLineCommand : ICommand
	{
		public string CommandName => "Delete line. Use del n where n - index ";

		public string Command => "del";

		public ILogger Logger { get; set; }

		public DeleteLineCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}
		public void Execute(ICore core, string[] command)
		{
			if (command[1].IsDigit() && command.Length == 2)
			{
				core.FileLines.Delete(Convert.ToInt32(command[1]));
				core.FileChanged = true;
				Logger.LogInformation($"Line delete index: {command[1]}");
			}

			Console.WriteLine("Line deleted");
		}
	}

}