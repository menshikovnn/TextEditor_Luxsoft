﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Clear console.
	/// </summary>
	public class ClearCommand : ICommand
	{
		public string CommandName => "Clear console";

		public string Command => "clear";
		public ILogger Logger { get; set; }

		public ClearCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}

		public void Execute(ICore core, string[] command)
		{
			Console.Clear();
		}
	}
}
