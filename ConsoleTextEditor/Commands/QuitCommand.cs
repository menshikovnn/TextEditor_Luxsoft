﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Text;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// Application close.
	/// </summary>
	public class QuitCommand: ICommand 
	{
		public string CommandName => "Exit programm";

		public string Command => "quit";

		public ILogger Logger { get; set; }
		public QuitCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}
		public void Execute(ICore core, string[] command)
		{
			if (core.FileChanged)
			{
				Console.WriteLine($"{core.Prefics} file was changed! If you are sure press Y! ");
				var key  = Console.ReadKey();
				if (key.KeyChar == 'Y' || key.KeyChar == 'y')
				{
					core.Stop = false;
				}
				else
				{
					Console.WriteLine($"{core.Prefics} Use save command for save file");
				}
			}
			else
			{
				core.Stop = false;
			}
		}
	}
}
