﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor.Commands
{
	/// <summary>
	/// List all lines command.
	/// </summary>
	public class ListCommand : ICommand
	{
		public string CommandName => "Display lines";

		public string Command => "list";

		public ILogger Logger { get; set; }
		public ListCommand(ILogger<Core> logger)
		{
			Logger = logger;
		}

		public void Execute(ICore core, string[] command)
		{
			int i = 0;
			foreach (var item in core.FileLines)
			{
				Console.WriteLine($"{core.Prefics} {++i}: {item} ");
			}
		}
	}
}