﻿using ConsoleTextEditor.Commands;
using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace ConsoleTextEditor
{
	class Program
	{
		public static IServiceProvider ServiceProvider { get; private set; }
		private static IServiceCollection serviceCollection;

		static void Main(string[] args)
		{
			serviceCollection = new ServiceCollection();
			ServiceProvider = ConfigureServices(serviceCollection);
			var core = ServiceProvider.GetService<ICore>();
			if (args.Length > 0)
			{
				core.Start(args[0]);
			}
			else 
			{
				core.Start();
			}
		}

		/// <summary>
		/// Create DI.
		/// </summary>
		/// <param name="serviceCollection"></param>
		/// <returns></returns>
		private static IServiceProvider ConfigureServices(IServiceCollection serviceCollection)
		{
			serviceCollection.AddSingleton<ICore, Core>();
			serviceCollection.AddTransient<ICommand, QuitCommand>();
			serviceCollection.AddSingleton<ICommand, HelpCommand>();
			serviceCollection.AddSingleton<ICommand, ClearCommand>();
			serviceCollection.AddSingleton<ICommand, AddLineCommand>();
			serviceCollection.AddSingleton<ICommand, DeleteLineCommand>();
			serviceCollection.AddSingleton<ICommand, ListCommand>();
			serviceCollection.AddSingleton<ICommand, SaveCommand>();
			serviceCollection.AddLogging(config =>
			{
				config.AddFile($"Logs/{DateTime.Now.ToShortDateString()}.txt");
			});
	
			return serviceCollection.BuildServiceProvider();
		} 
	}
}

