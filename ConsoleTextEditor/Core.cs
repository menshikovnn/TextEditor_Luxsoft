﻿using ConsoleTextEditor.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleTextEditor
{
	public class Core : ICore
	{
		#region Properties
		/// <summary>
		/// Prefics for UI.
		/// </summary>
		public string Prefics { get; private set; }
		
		/// <summary>
		/// Stop flag.
		/// </summary>
		public bool Stop { get; set; }

		/// <summary>
		/// File string.
		/// </summary>
		public LinkedList<string> FileLines{ get; set; }

		/// <summary>
		/// File changed flag.
		/// </summary>
		public bool FileChanged { get; set; }

		/// <summary>
		/// File name;
		/// </summary>
		public string FileName { get; private set; }
		
		/// <summary>
		/// Logger.
		/// </summary>
		public ILogger Logger { get; private set; }

		#endregion

		public Core(ILogger<Core> logger)
		{
			Logger = logger;
			FileLines = new LinkedList<string>();
			Prefics = ">>";
			Stop = true;
			Logger.LogInformation($"Start application {DateTime.Now}");
		}
		public void Start(string fileName)
		{
			FileName = fileName;
			LoadFile(fileName);
			StartUI();
		}
		public void Start()
		{
			StartUI();
		}

		#region Private
		/// <summary>
		/// Load lines from file;
		/// </summary>
		/// <param name="fileName">Input file.</param>
		private void LoadFile(string fileName)
		{
			if (File.Exists(fileName))
			{
				var lines = File.ReadAllLines(fileName);
				foreach (var item in lines)
					FileLines.AddLast(item);
				FileName = fileName;
				Logger.LogInformation($"File :{ fileName} loaded.");

			}
			else 
			{
				Console.Write($"{Prefics} Error! File not found.");
				Logger.LogError($"File:{ fileName }not found.");
				Stop = false;
				Logger.LogInformation($"Close application {DateTime.Now}");
			}
		}
		
		/// <summary>
		/// Start UI.
		/// </summary>
		private void StartUI()
		{
			Console.Write(Prefics);
			while (Stop)
			{
				var line = Console.ReadLine();
				if(line.Length > 0)
				{
					var args = line.Split(' ');
					var command = Program.ServiceProvider.GetServices(typeof(ICommand)).FirstOrDefault(x => ((ICommand)x).Command == args[0]) as ICommand;
					command?.Execute(this, args);
					Console.Write(Prefics);
				}
				else
				{
					Console.Write(Prefics);
				}
			}
		}
		#endregion
	}
}
